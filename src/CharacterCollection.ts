import OrderBy from './OrderBy';

class CharacterCollection extends OrderBy {
  public collection: string;
  public length: number;

  constructor(data: string) {
    super();
    this.collection = data;
    this.length = data.length;
  }

  // TODO
  ascending(): void {
    let arr = this.collection.split('');
    arr = arr.sort((a, b) => a < b ? -1 : 1)
    this.collection = arr.join('');
  }

  descending(): void {
    let arr = this.collection.split('');
    arr = arr.sort((a, b) => a < b ? 1 : -1)
    this.collection = arr.join('');
  }

}

export default CharacterCollection;
