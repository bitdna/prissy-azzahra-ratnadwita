import OrderBy from './OrderBy';

class Node {
  next: Node | null = null;
  data: number;

  constructor(data: number) {
    this.data = data;
  }
}

class LinkedList extends OrderBy {
  head: Node | null = null;

  constructor() {
    super();
  }

  get length(): number {
    if (!this.head) {
      return 0;
    }

    let counter = 1;
    let node = this.head;

    while (node.next) {
      counter++;
      node = node.next;
    }

    return counter;
  }

  add(data: number): void {
    const node = new Node(data);

    if (!this.head) {
      this.head = node;
      return;
    }

    let tail = this.head;

    while (tail.next) {
      tail = tail.next;
    }

    tail.next = node;
  }

  // TODO

  ascending(): void {
    let current: Node | null = this.head;
    let index: Node | null = null;

    while (current) {
      index = current.next;
      while (index) {
        if (current.data > index.data) {
          let temp = current.data;
          current.data = index.data;
          index.data = temp;
        }
        index = index.next;
      }
      current = current.next;
    }
  }

  descending(): void {
    let current: Node | null = this.head;
    let index: Node | null = null;

    while (current) {
      index = current.next;
      while (index) {
        if (current.data < index.data) {
          let temp = current.data;
          current.data = index.data;
          index.data = temp;
        }
        index = index.next;
      }
      current = current.next;
    }
  }

  print(): void {
    if (!this.head) {
      return;
    }

    let node: Node | null = this.head;
    let res = [];
    while (node) {
      res.push(node.data);
      node = node.next;
    }

    console.log(res.join(" -> "));
  }
}

export default LinkedList;
